<?php
namespace FiliWP\WP_Post_Types;

class WP_Post_Types
{
    private array $post_types = [];
    private array $excluded_post_types = [];

    public function __construct()
    {
        $this->exclude_wp_core_post_types();
    }

    private function fetch_all_post_types()
    {
        $post_types = get_post_types();

        return $post_types;
    }

    public function get_post_types()
    {
        return $this->post_types;
    }

    public function get_excluded_post_types()
    {
        return $this->excluded_post_types;
    }

    public function exclude_post_type(string $post_type)
    {
        if (!post_type_exists($post_type)) {
            $message = sprintf('The post type %s does not exist.', esc_attr($post_type));
            throw new Exception($message);
        }

        if (!in_array($post_type, $this->excluded_post_types)) {
            $this->excluded_post_types[] = $post_type;
        }
    }

    public function exclude_post_types(array $post_types)
    {
        foreach ($post_types as $post_type) {
            $this->exclude_post_type($post_type);
        }
    }

    private function get_excluded_wp_core_post_types()
    {
        $post_types = [
            'revision',
            'nav_menu_item',
            'custom_css',
            'customize_changeset',
            'oembed_cache',
            'user_request',
            'wp_block',
            'wp_template',
            'wp_template_part',
            'wp_global_styles',
            'wp_navigation',
            'wp_font_family',
            'wp_font_face',
        ];

        return $post_types;
    }

    private function exclude_wp_core_post_types()
    {
        $excluded_post_types = $this->get_excluded_wp_core_post_types();

        foreach ($excluded_post_types as $post_type) {
            $this->exclude_post_type($post_type);
        }
    }

    private function get_post_type_data(string $post_type)
    {
        $posts = get_posts([
            'post_type' => $post_type,
            'post_status' => 'any',
            'numberposts' => -1,
        ]);

        $total_posts = count($posts);

        $published_posts = count(array_filter($posts, function ($post) {
            return $post->post_status === 'publish';
        }));

        $draft_posts = count(array_filter($posts, function ($post) {
            return $post->post_status === 'draft';
        }));

        $private_posts = count(array_filter($posts, function ($post) {
            return $post->post_status === 'private';
        }));

        $post_type_data = [
            'post_type' => $post_type,
            'total_posts' => $total_posts,
            'published_posts' => $published_posts,
            'draft_posts' => $draft_posts,
            'private_posts' => $private_posts,
        ];

        return $post_type_data;
    }

    public function get_all_post_type_data(): array
    {
        $all_post_types = $this->fetch_all_post_types();

        if (!$all_post_types) {
            return [];
        }

        $excluded_post_types = $this->get_excluded_post_types();

        $post_types = array_diff($all_post_types, $excluded_post_types);
        $post_types = apply_filters('post_types/post_types', $post_types);

        $post_type_data_array = [];

        foreach ($post_types as $post_type) {
            $post_type_data = $this->get_post_type_data($post_type);
            $post_type_data_array[] = $post_type_data;
        }

        return $post_type_data_array;
    }
}
